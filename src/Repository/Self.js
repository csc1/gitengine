/*
| The data about a repository (filesystem, not cscdata)
*/
def.attributes =
{
	// branches in the repository
	branches: { type: [ 'undefined', 'group@Branch' ], },

	// branch to couple ( empty, '' or undefined is "master").
	couplingBranch: { type: [ 'undefined', 'string' ], },

	// the dir to couple ( empty, '' or undefined is whole repository).
	couplingDir: { type: [ 'undefined', 'string' ], },

	// the url to couple to.
	couplingUrl: { type: [ 'undefined', 'string' ], },

	// coupling syncronization
	couplingSemaphore: { type: [ 'undefined', 'Util/Semaphore' ], },

	// the repository description
	description: { type: [ 'undefined', 'string' ], },

	// groups and their permissions ( keys username, values 'r' or 'rw' )
	groups: { type: 'group@string' },

	// repository name
	name: { type: 'string' },

	// path on disk
	path: { type: 'string' },

	// users and their permissions ( keys username, values 'r' or 'rw' )
	users: { type: 'group@string' },
};

def.alike =
{
	alikeIgnoringBranches:
	{
		ignores: { 'branches': true },
	}
};

import { Self as Branch      } from '{Branch}';
import { Self as GroupBranch } from '{group@Branch}';
import { Self as User        } from '{User/Self}';
import { Self as Git         } from '{Util/Git}';

/*
| Sees what permissions the 'user' has on this file.
|
| ~user: user data object.
|
| ~return: 'rw', 'r' or false.
*/
def.proto.getPermissions =
	function( user )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( user.ti2ctype !== User ) throw new Error( );
/**/}

	let perms = this.users.get( user.username ) || false;

	const groups = this.groups;
	for( let gname of user.groups )
	{
		const gperms = groups.get( gname );
		if( !gperms ) continue;

		if( !perms || gperms.length > perms.length ) perms = gperms;
	}

	return perms;
};

/*
| Reads the branches of this repository from disk.
|
| FIXME, remove???
|
| ~return repo with branches (re)created.
*/
def.proto.readBranches =
	async function( )
{
	const data =
		await Git.command(
			this.path,
			'--no-pager',
			'branch',
			'-al',
			'--format=%(refname:short) %(objectname)',
		);
	const lines = data.split( '\n' );
	const bTable = { };
	for( let line of lines )
	{
		line = line.trim( );
		if( line === '' ) continue;

		const cols = line.split( ' ' );
		const refName = cols[ 0 ];
		const commitSha = cols[ 1 ];
		bTable[ refName ] =
			Branch.create(
				'name', refName,
				'lastCommitHash', commitSha,
			);
	}

	return this.create( 'branches', GroupBranch.Table( bTable ) );
};
