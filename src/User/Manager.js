/*
| Manages the users.
*/
def.abstract = true;

import { Self as CGit      } from '{Https/CGit}';
import { Self as GroupUser } from '{group@User/Self}';
import { Self as User      } from '{User/Self}';

let _users;

/*
| Initalizes the group manager.
*/
def.static.init =
	function( )
{
	_users = GroupUser.Empty;
};

/*
| Gets a user.
*/
def.static.get = ( username ) => _users.get( username );

/*
| Removes a user.
*/
def.static.remove =
	function( username )
{
	_users = _users.remove( username );
	CGit.invalidate( username );
};

/*
| Sets a user.
*/
def.static.set =
	function( user )
{
/**/if( CHECK && user.ti2ctype !== User ) throw new Error( );

	_users = _users.set( user.username, user );
	CGit.invalidate( user.username );
};

/*
| Returns the users.
*/
def.static.users = ( ) => _users;
