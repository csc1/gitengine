/*
| Converts a v1 catalog to v2.
*/
def.abstract = true;

import { default as level } from 'level';

import { Self as LfsFile } from '{Lfs/File/Self}';

/*
| Prints out usage info.
*/
function usage( )
{
	const argv = process.argv;
	console.log( 'USAGE: ' + argv[ 0 ] + ' ' + argv[ 1 ] + ' [FROM-DB-DIR] [TO-DB-DIR]' );
}

/*
| Main runner.
*/
def.static.init =
	async function( )
{
	if( process.argv.length !== 4 )
	{
		usage( );
		return;
	}

	const dbFrom = new level.Level( process.argv[ 2 ] );
	await dbFrom.open( );
	const version = await dbFrom.get( 'version' );
	if( version !== 'git-engine-1' )
	{
		throw new Error( 'invalid FromDb version' );
	}

	const dbTo = new level.Level( process.argv[ 3 ] );
	await dbTo.open( );
	await dbTo.put( 'version', 'gitengine-lfs-catalog-2' );

	const it = dbFrom.iterator( );
	for await( const [ key, value ] of it )
	{
		if( key === 'version' ) continue;
		if( !key.startsWith( 'obj:' ) ) throw new Error( );

		const json = JSON.parse( value );
		json.repositories.$type = 'set@string';

		const lfsFile = LfsFile.FromJson( json );
		await dbTo.put( key, lfsFile.jsonfy( ) );
	}

	console.log( 'DONE' );
};
