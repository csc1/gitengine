/*
| Converts the catalog from v1 to v2
*/
global.CHECK = true;
global.NODE = true;

await import( 'ti2c' );
await import( 'ti2c-web' );
await import( '@csc1/passlock' );

const pkg =
	await ti2c.register(
		'vcs', import.meta,
		'src/', 'Tools/ConvertDb/Start', 'codegen/'
	);
const Root = await pkg.import( 'Tools/ConvertDb/Root' );
await Root.init( );
