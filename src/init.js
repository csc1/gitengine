// global ti2c options
if( global.NODE === undefined ) global.NODE = true;
if( global.CHECK === undefined ) global.CHECK = true;

await import( '@csc1/passlock' );
let gitengine;

const pkg =
	await ti2c.register(
		'name',    'gitengine',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', 'init'
	);

export const init =
	async ( ) =>
{
	gitengine = await pkg.import( 'Self' );
	gitengine._init( pkg.rootDir );
};

export { gitengine as default };
export { gitengine as GitEngine };
