/*
| Provides the ssh interface for git repositories.
*/
def.abstract = true;

import { Buffer } from 'node:buffer';

//import { Self as FileData          } from '{Lfs/File/Self}';
import { Self as LfsManager        } from '{Lfs/Manager}';
import { Self as Log               } from '{Log/Self}';
import { Self as RepositoryManager } from '{Repository/Manager}';
import { Self as Token             } from '{Lfs/Token}';

/*
| Handles a LFS authentication request.
| Gives a token to be used against LFS API.
*/
def.static.authenticate =
	function( count, path, user, accept, reject )
{
	Log.log( 'ssh-lfs', count, 'client wants to authenticate: ' + path );

	const split = path.split( ' ' );
	if( split.length !== 2 )
	{
		Log.log( 'ssh-lfs', count, 'invalid path' );
		reject( );
		return;
	}

	let repoName = split[ 0 ];
	const operation = split[ 1 ];

	if( repoName.endsWith( '.git' ) )
	{
		repoName = repoName.substr( 0, repoName.length - 4 );
	}

	while( repoName.startsWith( '/' ) )
	{
		repoName = repoName.substr( 1 );
	}

	while( repoName.endsWith( '/' ) )
	{
		repoName = repoName.substring( 0, repoName.length - 1 );
	}

	const repo = RepositoryManager.get( repoName );
	if( !repo )
	{
		Log.log( 'ssh-lfs', count, 'repo does not exist' );
		reject( );
		return;
	}

	const perms = repo.getPermissions( user );

	if( !perms )
	{
		Log.log(
			'ssh-lfs', count,
			'user ', user.username + ' has no access to ' + repoName + '.git'
		);
		reject( );
		return;
	}

	switch( operation )
	{
		case 'download':
			break;

		case 'upload':
			if( perms !== 'rw' )
			{
				Log.log(
					'ssh-lfs', count,
					'repo ' + repoName + '.git is readonly to user'
				);
				reject( );
				return;
			}
			break;

		default:
			Log.log( 'ssh-lfs', count, 'invalid operation' );
			reject( );
			return;
	}

	const stream = accept( );
	Log.log(
		'ssh-lfs', count,
		'user ' + user.username + ' lfs authenticated '
		+ repoName + '.git (' + operation + ')'
	);
	const token = LfsManager.getUserToken( user.username );

	const auth64 = new Buffer.from( user.username + ':' + token.token ).toString( 'base64' );

	stream.stdout.write(
		JSON.stringify(
			{
				// href: 'https://localhost:8443/' + repoName + '.git/info/lfs',
				header:
				{
					'Authorization': 'RemoteAuth ' + auth64,
				},
				expires_in: Token.lifetime,
			}
		)
	);
	stream.exit( 0 );
	stream.end( );
};

/*
| Handles a LFS transfer request.
*/
/*
def.static.transfer =
	function( count, path, user, accept, reject )
{
	Log.log( 'ssh-lfs', count, 'client wants to transfer: ' + path );

	const split = path.split( ' ' );
	if( split.length !== 2 )
	{
		Log.log( 'ssh-lfs', count, 'invalid path' );
		reject( );
		return;
	}

	let repoName = split[ 0 ];
	const operation = split[ 1 ];

	if( repoName.endsWith( '.git' ) )
	{
		repoName = repoName.substr( 0, repoName.length - 4 );
	}

	while( repoName.startsWith( '/' ) )
	{
		repoName = repoName.substr( 1 );
	}

	while( repoName.endsWith( '/' ) )
	{
		repoName = repoName.substring( 0, repoName.length - 1 );
	}

	const repo = RepositoryManager.get( repoName );
	if( !repo )
	{
		Log.log( 'ssh-lfs', count, 'repo does not exist' );
		reject( );
		return;
	}

	const perms = repo.getPermissions( user );

	if( !perms )
	{
		Log.log(
			'ssh-lfs', count,
			'user ', user.username + ' has no access to ' + repoName + '.git'
		);
		reject( );
		return;
	}

	switch( operation )
	{
		case 'download':
			break;

		case 'upload':
			if( perms !== 'rw' )
			{
				Log.log(
					'ssh-lfs', count,
					'repo ' + repoName + '.git is readonly to user'
				);
				reject( );
				return;
			}
			break;

		default:
			Log.log( 'ssh-lfs', count, 'invalid operation' );
			reject( );
			return;
	}

	const stream = accept( );
	Log.log(
		'ssh-lfs', count,
		'user ' + user.username + ' lfs transfers '
		+ repoName + '.git (' + operation + ')'
	);

	// step 1 anounce
	write( stream, count, pack( 'version=1\n' ) + '0000' );

	let state =
	{
		p: 'start',
		operation: operation,
		repoName: repoName,
		version: undefined,
		args: undefined,
		oids: undefined,
	};

	let data = '';
	stream.stdin.on(
		'data',
		async ( chunk ) =>
	{
		data += chunk;
		console.log( 'CHUNK', chunk + '' );

		for(;;)
		{

			if( data.length < 4 ) return;

			const plen = parseInt( data.substring( 0, 4 ), 16 );
			if( data.length < plen ) return;

			let line;
			if( plen > 4 )
			{
				console.log( count + '  IN:' + data.substring( 0, plen ).replaceAll( '\n', '\\n' ) );
				line = data.substring( 4, plen );
				data = data.substring( plen );
			}
			else if( plen === 0 )
			{
				// flush
				console.log( count + '  IN:0000'  );
				line = true;
				data = data.substring( 4 );
			}
			else if( plen === 1 )
			{
				// delimiter
				console.log( count + '  IN:0001' );
				line = false;
				data = data.substring( 4 );
			}
			else
			{
				Log.log( 'ssh-lfs', count, 'protocol fail' );
				stream.exit( -1 );
				stream.end( );
				return;
			}

			const r = await handleTransferLine( stream, count, state, line );
			if( r === false )
			{
				return;
			}
		}
	}
	);
};
*/

/*
| Handles a batch command in a ssh-lfs-transfer protocol.
*/
/*
async function handleTransferBatchUpload( stream, count, state )
{
	write( stream, count, pack( 'status 200\n' ) );
	write( stream, count, '0001' );
	for( let e of state.oids )
	{
		// removing LF
		e = e.substring( 0, e.length - 1 );
		const split = e.split( ' ' );
		const oid = split[ 0 ];
		const size = parseInt( split[ 1 ], 0 );
		if( FileData.checkOidSize( count, oid, size ) )
		{
			const lfData = await LfsManager.download( oid, size );
			if( !lfData.uploaded )
			{
				write(
					stream, count,
					pack( oid + ' ' + size + ' upload\n' )
				);
			}
		}
	}
	write( stream, count, '0000' );
}
*/

/*
| Handles a batch command in a ssh-lfs-transfer protocol.
*/
/*
async function handleObjectUpload( stream, count, state )
{
	const oid = state.oid;
	let size;
	for( let arg of state.args )
	{
		const ioe = arg.indexOf( '=' );
		if( ioe < 0 ) continue;
		const key = arg.substring( 0, ioe );
		const val = arg.substring( ioe + 1 );
		if( key.toUpperCase( ) === 'SIZE' ) size = parseInt( val, 10 );
	}

	if( size === undefined )
	{
		Log.log( 'ssh-lfs', count, 'size missind' );
		write( stream, count, pack( 'status 400\n' ) + '0000' );
		stream.exit( -1 );
		stream.end( );
		return false;
	}

	const lfData = await LfsManager.upload( oid, size, state.repoName );
	console.log( 'LFDATA', lfData );
}
*/

/*
| Handles a line in a ssh-lfs-transfer protocol.
*/
/*
async function handleTransferLine( stream, count, state, line )
{
	switch( state.p )
	{
		case 'batch-args':
		{
			if( line === true )
			{
				state.p = 'ready';
				await handleTransferBatchUpload( stream, count, state );
				return;
			}

			if( line === false )
			{
				state.p = 'batch-oids';
				return;
			}
			state.args.push( line );
			return;
		}

		case 'batch-oids':
		{
			if( line === true )
			{
				state.p = 'ready';
				await handleTransferBatchUpload( stream, count, state );
				return;
			}

			if( line === false )
			{
				Log.log( 'ssh-lfs', count, 'double delim' );
				stream.exit( -1 );
				stream.end( );
				return false;
			}
			state.oids.push( line );
			return;
		}

		case 'list-lock':
		{
			if( line === true )
			{
				write( stream, count, pack( 'status 501\n' ) + '0000' );
				state.p = 'ready';
				return;
			}
			else
			{
				// ignore
				return;
			}
		}

		case 'put-object':
		{
			if( line === true )
			{
				Log.log( 'ssh-lfs', count, 'expected arg or delim' );
				stream.exit( -1 );
				stream.end( );
				return false;
			}
			else if( line === false )
			{
				console.log( 'UPLOADING' );
				state.p = 'uploading';
				await handleObjectUpload( stream, count, state );
				return;
			}
			else
			{
				state.args.push( line );
				return;
			}
		}

		case 'ready':
		{
			if( line === 'list-lock\n' )
			{
				state.p = 'list-lock';
				return;
			}
			else if( line === 'batch\n' )
			{
				state.p = 'batch-args';
				state.args = [ ];
				state.oids = [ ];
				return;
			}
			else if( line.substring( 0, 11 ) === 'put-object ' )
			{
				if( state.operation !== 'upload' )
				{
					Log.log( 'ssh-lfs', count, 'put-object under invalid operation' );
					write( stream, count, pack( 'status 400\n' ) + '0000' );
					stream.exit( -1 );
					stream.end( );
					return false;
				}
				state.p = 'put-object';
				state.oid = line.substring( 11, line.length - 1 );
				state.args = [ ];
				return;
			}
			else
			{
				return false;

			}
		}

		case 'start':
		{
			state.version = line;
			state.p = 'startflush';
			break;
		}

		case 'startflush':
		{
			if( line !== true )
			{
				Log.log( 'ssh-lfs', count, 'expected flush' );
				stream.exit( -1 );
				stream.end( );
				return false;
			}

			if( state.version !== 'version 1\n' )
			{
				Log.log( 'ssh-lfs', count, 'unknown version on start' );
				write( stream, count, pack( 'status 400\n' ) + '0000' );
				stream.exit( -1 );
				stream.end( );
				return;
			}

			write( stream, count, pack( 'status 200\n' ) + '0000' );
			state.p = 'ready';
			return;
		}

		default: throw new Error( );
	}
}
*/

/*
| Wrapper to write on stream with logging
*/
/*
function write( stream, count, s )
{
	console.log( count + ' OUT:' + s.replaceAll( '\n', '\\n' ) );
	stream.stdout.write( s );
}
*/

/*
function pack( s )
{
	const n = ( 4 + s.length ).toString( 16 );
	return Array( 4 - n.length + 1 ).join( '0' ) + n + s;
}
*/
