/*
| A gitengine extension downsync a repo from overleaf upon ssh request
| (upsync is not necessary because this happens on git-receive anyway)
*/
def.abstract = true;

import { Self as Coupling          } from '{Coupling/Self}';
import { Self as Log               } from '{Log/Self}';
import { Self as RepositoryManager } from '{Repository/Manager}';

/*
| Handles a git request (upload-pack or receive-pack).
|
| ~count: client counter
| ~path: path of ssh command
| ~user: authenticated user
| ~accept: accept call of ssh2 library
| ~reject: reject call of ssh2 library
*/
def.static.serve =
	async function( count, path, user, accept, reject )
{
	if( path.endsWith( '.git' ) )
	{
		path = path.substring( 0, path.length - 4 );
	}
	const repo = RepositoryManager.get( path );
	if( !repo )
	{
		Log.log( 'ssh-couple', count, 'repo does not exist' );
		const stream = accept( );
		stream.exit( -1 );
		stream.end( );
		return;
	}
	const perms = repo.getPermissions( user );
	if( !perms )
	{
		Log.log(
			'ssh-couple', count,
			'user ' + user.username + ' has no access to ' + path + '.git'
		);
		const stream = accept( );
		stream.exit( -1 );
		stream.end( );
		return;
	}
	const stream = accept( );
	const dsResult = await Coupling.downSync( count, path );
	if( !dsResult )
	{
		try
		{
			const msg = 'ERR Remote coupling failed';
			stream.stdout.write( msg );
			stream.exit( -1 );
			stream.end( );
			return;
		}
		catch( e )
		{
			return;
		}
	}
	const msg = 'OK';
	stream.stdout.write( msg );
	stream.exit( 0 );
	stream.end( );
};
