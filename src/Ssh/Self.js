/*
| Provides the ssh interface.
*/

def.abstract = true;

import crypto from 'crypto';
import ssh2 from 'ssh2';

import { Self as LfsManager  } from '{Lfs/Manager}';
import { Self as Log         } from '{Log/Self}';
import { Self as SshCouple   } from '{Ssh/Couple}';
import { Self as SshGit      } from '{Ssh/Git}';
import { Self as SshLfs      } from '{Ssh/Lfs}';
import { Self as UserManager } from '{User/Manager}';

/*
| block these ips.
*/
let _blockList = new Set( );

/*
| Configured ips.
*/
let _ips = [ '0.0.0.0' ];

/*
| Port to listen to.
*/
let _port = 22;

/*
| Host keys.
*/
let _hostKeys;

/*
| Parsed ssh keys.
*/
let _ssh2ParsedMap = new Map( );

/*
| Adds an ip to blocklist.
*/
def.static.block =
	function( ip )
{
	_blockList.add( ip );
};

/*
| Sets the host keys.
*/
def.static.setHostKeys =
	function( keys )
{
	_hostKeys = keys;
};

/*
| Sets IPs to listen to.
|
| ~ips: array of strings.
*/
def.static.setIPs =
	function( ips )
{
	_ips = ips;
};

/*
| Sets Port to listen to.
|
| ~port: port
*/
def.static.setPort =
	function( port )
{
	_port = port;
};

/*
| Time save password checking.
*/
def.static._checkValue =
	function( input, allowed )
{
	const autoReject = ( input.length !== allowed.length );
	if( autoReject )
	{
		// Prevent leaking length information by always making a comparison with the
		// same input when lengths don't match what we expect ...
		allowed = input;
	}
	const isMatch = crypto.timingSafeEqual( input, allowed );
	return !autoReject && isMatch;
};

/*
| A ssh client wants to authenticate.
|
| ~count: client counter
| ~ctx: ssh2 context
*/
def.static._sshAuth =
	function( count, ctx )
{
	if( ctx.method !== 'publickey' )
	{
		Log.debug( 'ssh', count, 'ctx.method not "publickey" but:', ctx.method );
		return ctx.reject( [ 'none', 'publickey' ] );
	}

	const username = ctx.username;
	const user = UserManager.get( username );
	if( !user )
	{
		Log.log( 'ssh', count, 'user "' + username + '" unknown' );
		return ctx.reject( [ 'publickey' ] );
	}

	const sshKeys = user.sshKeys;
	if( !sshKeys )
	{
		Log.log( 'ssh', count, 'user "' + username + '" has no ssh keys"' );
		return ctx.reject( [ 'publickey' ] );
	}

	let allowed = false;
	for( let sshKey of sshKeys )
	{
		if( ctx.key.algo !== sshKey.algorithm ) continue;

		let ssh2Parsed = _ssh2ParsedMap.get( sshKey );
		if( !ssh2Parsed )
		{
			ssh2Parsed = ssh2.utils.parseKey( sshKey.asText );
			_ssh2ParsedMap.set( sshKey, ssh2Parsed );
		}

		if( !ssh2Parsed.getPublicSSH ) continue;

		if( !Self._checkValue( ctx.key.data, ssh2Parsed.getPublicSSH( ) ) ) continue;

		if( ctx.signature && ssh2Parsed.verify( ctx.blob, ctx.signature, ctx.hashAlgo ) !== true ) continue;

		allowed = true;
		break;
	}

	if( !allowed )
	{
		Log.debug( 'user "' + username + '" has no valid ssh key' );
		return ctx.reject( [ 'publickey' ] );
	}

	this.user = user;
	ctx.accept( );
};

/*
| Starts the ssh server.
*/
def.static.start =
	async function( )
{
	Log.log( 'ssh', '*', 'starting' );

	if( !_hostKeys )
	{
		throw new Error( 'no ssh host keys provided' );
	}

	const connected =
		( client, info ) =>
	{
		const count = Log.getCount( );
		let nextSubCount = 1;

		if( _blockList.has( info.ip ) )
		{
			Log.log( 'ssh', count, 'client blocked:', info.ip );
			client.end( );
			return;
		}

		Log.log( 'ssh', count, 'client connected', info );

		client
		.on( 'authentication',
			function( ctx )
			{
				Self._sshAuth.call( this, count, ctx );
			}
		)
		.on( 'ready', ( ) => {
			Log.debug( 'ssh', count, 'client authenticated!' );
			client.on(
				'session',
				function( accept, reject )
				{
					Self._sshSession(
						count + ':' + nextSubCount++,
						this.user, accept, reject
					);
				}
			);
		} )
		.on( 'close', ( ) => {
			Log.log( 'ssh', count, 'client disconnected' );
		} )
		.on( 'error', ( err ) => {
			Log.log( 'ssh', count, 'client error', err );
			Log.log( 'ssh', count, 'client info', info );
		} );
	};

	const config = { hostKeys: _hostKeys };
	if( Log.debugging ) config.debug = Log.debug;

	for( let ip of _ips )
	{
		const server = new ssh2.Server( config, connected );
		server.listen(
			_port, ip,
			function( )
			{
				const address = this.address( );
				Log.log( 'ssh', '*', 'listening on ' + address.address + ':' + address.port );
			}
		);
	}
};

/*
| A ssh session.
|
| ~count: client counter
| ~user: authenticated user
| ~sessionAccept: ssh2 library accept call
| ~sessionReject: ssh2 library reject call
*/
def.static._sshSession =
	function( count, user, sessionAccept, sessionReject )
{
	const session = sessionAccept( );
	session.once(
		'exec',
		( accept, reject, info ) =>
	{
		const command = info.command;
		if( command.substr( 0, 16 ) === 'git-upload-pack ' )
		{
			SshGit.serve(
				count,
				'git-upload-pack', command.substr( 16 ),
				user, accept, reject
			);
			return;
		}
		else if( command.substr( 0, 17 ) === 'git-receive-pack ' )
		{
			SshGit.serve(
				count,
				'git-receive-pack', command.substr( 17 ),
				user, accept, reject
			);
			return;
		}
		else if( command.substr( 0, 21 ) === 'git-lfs-authenticate ' )
		{
			if( !LfsManager.enabled( ) )
			{
				Log.log( 'ssh', count, 'LFS not enabled', command );
				const stream = accept( );
				stream.exit( -1 );
				stream.end( );
				return;
			}

			SshLfs.authenticate(
				count,
				command.substr( 21 ),
				user, accept, reject
			);
			return;
		}
		else if( command.substr( 0, 17 ) === 'git-lfs-transfer ' )
		{
			// git-lfs-transfer currently disabled / wip
			const stream = accept( );
			stream.exit( -1 );
			stream.end( );

			/*
			if( !LfsManager.enabled( ) )
			{
				Log.log( 'ssh', count, 'LFS not enabled', command );
				const stream = accept( );
				stream.exit( -1 );
				stream.end( );
				return;
			}

			SshLfs.transfer(
				count,
				command.substr( 17 ),
				user, accept, reject
			);
			*/
			return;
		}
		else if( command.substring( 0, 7 ) === 'couple ' )
		{
			// A gitengine extension downsync a repo from overleaf upon ssh request
			// (upsync is not necessary because this happens on git-receive anyway

			SshCouple.serve( count, command.substr( 7 ), user, accept, reject );
			return;
		}
		else
		{
			Log.log( 'ssh', count, 'unsupported command', command );
			const stream = accept( );
			stream.exit( -1 );
			stream.end( );
		}
	} );
};
