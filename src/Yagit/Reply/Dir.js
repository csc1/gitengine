/*
| The server replies a /dir request.
*/
def.attributes =
{
	// the entries in the dir
	entries: { type: 'list@Yagit/Dir/Entry', json: true }
};

def.json = 'ReplyDir';
