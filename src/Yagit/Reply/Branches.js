/*
| Branches info of a repository.
*/
def.attributes =
{
	// names keys, values are shas
	branches: { type: 'group@string', json: true }
};

def.json = 'ReplyBranches';
