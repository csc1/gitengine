/*
| The servers replies to a clients listing request.
*/
def.attributes =
{
	// the listing
	listing: { type: 'group@Yagit/Listing/Repository', json: true },
};

def.json = 'ReplyListing';
