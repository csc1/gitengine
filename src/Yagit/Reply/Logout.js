/*
| The servers replies to a succesfull clients logout request.
*/
def.singleton = true;
def.json = 'ReplyLogout';
