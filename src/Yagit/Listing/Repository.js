/*
| A repository in the listing.
*/
def.attributes =
{
	// the repository description
	description: { type: [ 'undefined', 'string' ], json: true },

	// repository name
	name: { type: 'string', json: true },
};

def.json = 'ListingRepository';
