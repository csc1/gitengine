/*
| The client requests it's session to be checked/authenticated.
*/
def.singleton = true;
def.json = 'RequestAuth';
