/*
| The client requests an user login.
*/
def.attributes =
{
	// the username
	username: { type: 'string', json: true },

	// the password
	password: { type: 'string', json: true },
};

def.json = 'RequestLogin';
