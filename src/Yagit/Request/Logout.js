/*
| The client requests it's session to be killed.
*/
def.singleton = true;
def.json = 'RequestLogout';
