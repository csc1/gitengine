/*
| A dir entry.
*/
def.attributes =
{
	name: { type: 'string', json: true },
	type: { type: 'string', json: true },
};

def.json = 'DirEntry';
