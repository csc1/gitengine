/*
| A line in a commit patch hunk.
*/
def.attributes =
{
	// old line number
	oldLineno: { type: 'number', json: true },

	// new line number
	newLineno: { type: 'number', json: true },

	// content
	content: { type: 'string', json: true },
};

def.json = true;
