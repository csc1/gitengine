/*
| A commit patch hunk.
*/
def.attributes =
{
	// lines of the hunk
	lines: { type: 'list@Yagit/Commit/Patch/Hunk/Line', json: true }
};

def.json = true;
