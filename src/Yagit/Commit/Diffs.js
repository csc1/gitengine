/*
| A git diffs (= a list of patches)
*/
def.attributes =
{
	patches: { type: 'list@Yagit/Commit/Patch/Self', json: true },
};

def.json = 'CommitDiffs';
