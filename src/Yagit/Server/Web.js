/*
| Handles yagit specific adaptions to ti2c-web.
*/
def.attributes =
{
	// the web bundle
	_bundle: { type: 'web:Bundle' },

	// hash of prism.js
	_prismHash: { type: [ 'undefined', 'string' ] },

	// hash of style.css
	_styleHash: { type: [ 'undefined', 'string' ] },

	// the ti2c-web instance
	_tw : { type : 'web:Self' }
};

import fs              from 'node:fs/promises';
import { hashElement } from 'folder-hash';

import { Self as Ajax           } from '{Yagit/Server/Ajax}';
import { Self as Branches       } from '{Yagit/Server/Branches}';
import { Self as Bundle         } from '{web:Bundle}';
import { Self as Diffs          } from '{Yagit/Server/Diffs}';
import { Self as Dir            } from '{Yagit/Server/Dir}';
import { Self as File           } from '{Yagit/Server/File}';
import { Self as GroupBoolean   } from '{group@boolean}';
import { Self as History        } from '{Yagit/Server/History}';
import { Self as Https          } from '{Https/Self}';
import { Self as Listing        } from '{Yagit/Server/Listing}';
import { Self as Log            } from '{Log/Self}';
import { Self as Path           } from '{Yagit/Path/Self}';
import { Self as ResourceFile   } from '{web:Resource/File}';
import { Self as ResourceMemory } from '{web:Resource/Memory}';
import { Self as ResourceTwig   } from '{web:Resource/Twig}';
import { Self as Ti2cWeb        } from '{web:Self}';

import '{Yagit/Client/Root}';

/*
| Prepares ti2c-web.
|
| ~absolute dir of root directory.
*/
def.static.prepare =
	async function( base )
{
	Log.log( 'yagit', '*', 'preparing ti2c-web' );

	let tw =
		Ti2cWeb.create(
			'log', console.log,
			'requestCaching', true,
		);

	// caclulates the style sheet hash
	const baseStyle = base.dir( 'media' ).dir( 'yagit' ).file( 'style.css' );
	let styleHash = await hashElement( baseStyle.asString, { } );
	styleHash = encodeURI( styleHash.hash );
	Log.log( 'yagit', '*', 'style hash: ' + styleHash );

	// calculates the pdfjs hash
	const aDirPdfjs = base.dir( 'pdfjs' );
	let pdfJsHash = await hashElement( aDirPdfjs.asString, { } );
	pdfJsHash = encodeURI( pdfJsHash.hash );
	Log.log( 'yagit', '*', 'pdfjs hash: ' + pdfJsHash );

	// calculates the prism hash
	const basePrism = base.dir( 'dist' ).dir( 'prism' );
	let prismHash = await hashElement( basePrism.asString, { } );
	prismHash = encodeURI( prismHash.hash );
	Log.log( 'yagit', '*', 'prism hash: ' + prismHash );

	const globalFlags =
		GroupBoolean.Table( {
			CHECK: true,
			NODE: false,
		} );

	let rt = await Self._roster( base, styleHash, pdfJsHash, prismHash );
	rt = await rt.prepare( );

	const bundle =
		await Bundle.build(
			'entry', 'gitengine:Yagit/Client/Root',
			'extra',
				ResourceTwig.create(
					'twig:add', 'client--extra-config.js',
					ResourceMemory.JsData(
						'globalThis.PDF_JS_HASH = "' + pdfJsHash + '";\n'
					)
				),
			'globalFlagsDevel', globalFlags,
			'globalFlagsIndex', globalFlags,
			'log', console.log,
			//'minify', false,
			'name', 'client',
			'offerIndex', true,
			'offerDevel', true,
			//'reportDir', base.d( 'report' ),
			//'sameOrigin', false,
		);

	rt = rt.appendTwig( bundle.scripts );
	rt = rt.appendTwig( bundle.combine );

	return(
		Self.create(
			'_bundle', bundle,
			'_prismHash', prismHash,
			'_styleHash', styleHash,
			'_tw', tw.create( 'resources', rt )
		)
	);
};

/*
| Serves a yagit request.
|
| ~count: client counter
| ~req: request
| ~res: result
| ~urlSplit: url splitted into parts
*/
def.proto.serve =
	async function( count, request, result, urlSplit )
{
	const tw = this._tw;
	const pathname = tw.pathname( request );
	if( pathname === undefined )
	{
		return Https.error( result, 400, 'invalid URL' );
	}

	const parts = pathname.split( '/' );
	const path = Path.String( pathname );

	if( parts.length === 1 )
	{
		let devel;
		switch( parts[ 0 ] )
		{
			case 'devel.html':
				devel = true;
				break;

			case 'index.html':
			case '':
				devel = false;
				break;
		}

		if( devel !== undefined )
		{
			const header =
			{
				'content-type': 'text/html',
				'cache-control': 'no-cache',
				'date': new Date( ).toUTCString( ),
			};
			result.writeHead( 200, header );
			const data = this._yagitIndexPage( devel );
			result.end( data, 'utf-8' );
			return;
		}
	}

	if( parts.length > 0 )
	{
		const p0 = parts[ 0 ];
		switch( p0 )
		{
			case 'ajax':
				Ajax.handle( request, result, path );
				return;

			case 'branches':
				Branches.handle( request, result, path );
				return;

			case 'diffs':
				Diffs.handle( request, result, path );
				return;

			case 'dir':
				Dir.handle( request, result, path );
				return;

			case 'listing':
				Listing.handle( request, result, path );
				return;

			case 'file':
				File.handle( request, result, path );
				return;

			case 'history':
				History.handle( request, result, path );
				return;
		}
	}

	tw.requestListener( request, result );
};

/*
| The basic roster.
*/
def.static._roster =
	async function( base, styleHash, pdfJsHash, prismHash )
{
	const aDirMediaYagit = base.d( 'media' ).d( 'yagit' ) ;
	const aDirPrism = base.d( 'dist' ).d( 'prism' ) ;

	let rt = ResourceTwig.create(
		'twig:add', styleHash + '-style.css',
		ResourceFile.APathLong( aDirMediaYagit.f( 'style.css' ) ),

		'twig:add', prismHash + '-prism.css',
		ResourceFile.APathLong( aDirPrism.f( 'prism.css' ) ),

		'twig:add', prismHash + '-prism-dev.css',
		ResourceFile.APathLong( aDirPrism.f( 'prism-dev.css' ) ),

		'twig:add', prismHash + '-prism.js',
		ResourceFile.APathLong( aDirPrism.f( 'prism.js' ) ),

		'twig:add', prismHash + '-prism-dev.js',
		ResourceFile.APathLong( aDirPrism.f( 'prism-dev.js' ) ),
	);

	// adds pdfjs
	const aDirPdfjs = base.dir( 'pdfjs' );
	const subDirNames =
	[
		'build',
		'web',
		'web/cmaps',
		'web/images',
		'web/standard_fonts'
	];

	const skips =
	{
		'LICENSE': true,
		'LICENSE_FOXIT': true,
		'LICENSE_LIBERATION': true,
	};

	for( let subDirName of subDirNames )
	{
		let subDir;

		subDir = aDirPdfjs;
		for( let dirName of subDirName.split( '/' ) )
		{
			subDir = subDir.dir( dirName );
		}

		const dir = await fs.readdir( subDir.asString, { withFileTypes: true } );

		for( let dirent of dir )
		{
			if( !dirent.isFile( ) ) continue;

			const filename = dirent.name;

			if( skips[ filename ] ) continue;
			if( filename.endsWith( '.swp' ) ) continue;

			rt = rt.create(
				'twig:add', 'pdfjs-' + pdfJsHash + '/' + subDirName + '/' + filename,
				ResourceFile.APathLong( subDir.f ( filename ) ),
			);
		}
	}

	return rt;
};

/*
| The index/devel.html page.
|
| ~devel: true builds the devel version.
*/
def.lazyFunc._yagitIndexPage =
	function( devel )
{
	const data = [ ];
	data.push(
		'<!DOCTYPE HTML>',
		'<html>',
		'<head>',
		'<meta charset="utf-8"/>',
		'<title>yagit</title>',
		'<link rel="stylesheet" href="/' + this._prismHash + '-prism.css" type="text/css"/>',
		'<link rel="stylesheet" href="/' + this._styleHash + '-style.css" type="text/css"/>',
		'<script type="text/javascript" src="/' + this._prismHash + '-prism.js"></script>',
	);

	if( devel )
	{
		const bundle = this._bundle;
		const scripts = bundle.scripts;
		data.push( bundle.importMap );
		for( let key of scripts.keys )
		{
			data.push( '<script src="/' + key + '" type="module"></script>' );
		}
	}
	else
	{
		data.push( '<script src="/' + this._bundle.name + '" type="module"></script>' );
	}

	data.push(
		'</head>',
		'<body>',
		'<div id="loadDiv" style="margin: 3em auto 0 auto; width: 10em">',
		'...Loading....',
		'</div>',
		'</body>',
	);

	return data.join( '' );
};

