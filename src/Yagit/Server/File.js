/*
| Handles ajax requests.
*/
def.abstract = true;

import { Self as Access            } from '{Yagit/Server/Access}';
import { Self as Git               } from '{Util/Git}';
import { Self as FileTypes         } from '{web:FileTypes}';
import { Self as Https             } from '{Https/Self}';
import { Self as LfsManager        } from '{Lfs/Manager}';
import { Self as RepositoryManager } from '{Repository/Manager}';

/*
| Gets the content type for 'filename'.
*/
def.static.contentType =
	function( filename )
{
	filename = filename.toLowerCase( );
	const iod = filename.lastIndexOf( '.' );

	if( iod < 0 ) return;
	const ext = filename.substr( iod + 1 );

	try { return FileTypes.mime( ext ); }
	catch( e ) { return undefined; }
};

/*
| Handles a file request.
*/
def.static.handle =
	async function( request, result, path )
{
	const parts = path.parts;
	const plen = parts.length;
	if( plen < 3 )
	{
		return Https.error( result, 404, 'file request too short' );
	}

/**/if( CHECK && parts.get( 0 ) !== 'file' ) throw new Error( );

	const repoName = parts.get( 1 );
	if( !Access.test( request, result, repoName ) ) return;
	// repo must exist otherwise access would have been denied

	const repo = RepositoryManager.get( repoName );
	const partCommitSha = parts.get( 2 ).trim( );
	if( partCommitSha[ 0 ] === '-' )
	{
		return Https.error( result, 404, 'invalid history request' );
	}

	const filename = path.chopn( 3 ).string;

	// gets the file contents
	// it is needed anyway, LFS pointer or direct data
	let data;
	try
	{
		data =
			await Git.commandBinary(
				repo.path,
				'--no-pager',
				'cat-file', 'blob',
				'--', partCommitSha + ':' + filename
			);
	}
	catch( e )
	{
		return Https.error( result, 500, 'internal server error' );
	}

	const contentType = Self.contentType( parts.last );
	const headers = { };
	if( contentType ) headers[ 'Content-Type' ] = contentType;

	// 28 days caching (git commit version shouldn't ever change)
	//headers[ 'Cache-Control' ] = 'max-age=2419200';
	headers[ 'Cache-Control' ] = 'max-age=86400';

	if( data.length <= 1024 )
	{
		// tests if the file is in LFS
		let dataCA;
		try
		{
			dataCA =
				await Git.command(
					repo.path,
					'--no-pager',
					'-c', 'attr.tree=' + partCommitSha,
					'check-attr', 'filter', '-z',
					'--', filename
				);
		}
		catch( e )
		{
			return Https.error( result, 500, 'internal server error' );
		}
		const dSplit = dataCA.split( '\0' );

		// pseudo 1-time loop to break of from.
		while( dSplit[ 2 ] === 'lfs' )
		{
			const lines = ( data + '' ).split( '\n' );
			if( lines < 3 ) break;
			if( lines[ 0 ] !== 'version https://git-lfs.github.com/spec/v1' ) break;

			const l2Parts = lines[ 1 ].split( ' ' );
			if( l2Parts < 2 ) break;
			if( l2Parts[ 0 ] !== 'oid' ) break;
			const lfsOid = l2Parts[ 1 ];

			const oidParts = lfsOid.split( ':' );
			if( oidParts.length < 2 ) break;

			const l3Parts = lines[ 2 ].split( ' ' );
			if( l3Parts[ 0 ] !== 'size' ) break;
			if( l3Parts < 2 ) break;
			const lfsSize = l3Parts[ 1 ];

			// it's a LFS object alright
			const lfData = await LfsManager.getLfData( oidParts[ 1 ] + ':' + lfsSize );
			if( !lfData ) break;

			return lfData.download( '#', request, result, headers );
		}
	}

	result.writeHead( 200, headers );
	result.end( data );
};
