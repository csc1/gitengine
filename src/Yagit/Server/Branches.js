/*
| Handles branches requests.
|
| Provides info about branches in a repository and their head commit.
*/
def.abstract = true;

import { Self as Access            } from '{Yagit/Server/Access}';
import { Self as Git               } from '{Util/Git}';
import { Self as GroupString       } from '{group@string}';
import { Self as Https             } from '{Https/Self}';
import { Self as ReplyBranches     } from '{Yagit/Reply/Branches}';
import { Self as RepositoryManager } from '{Repository/Manager}';

/*
| Handles a file request.
*/
def.static.handle =
	async function( request, result, path )
{
	const parts = path.parts;

	const plen = parts.length;
	if( plen !== 2 )
	{
		return Https.error( result, 404, 'invalid request length' );
	}

/**/if( CHECK && parts.get( 0 ) !== 'branches' ) throw new Error( );

	const repoName = parts.get( 1 );

	if( !Access.test( request, result, repoName ) ) return;

	const repo = RepositoryManager.get( repoName );
	// repo must exist otherwise access would have denied

	// FIXME use caching of the repository

	const data = await Git.command( repo.path, 'branch', '-al', '--format=%(refname:short) %(objectname)' );
	const lines = data.split( '\n' );
	const table = { };
	for( let line of lines )
	{
		line = line.trim( );
		if( line === '' ) continue;
		const cols = line.split( ' ' );
		const refName = cols[ 0 ];
		const commitSha = cols[ 1 ];
		table[ refName ] = commitSha;
	}

	const reply = ReplyBranches.create( 'branches', GroupString.Table( table ) );

	result.writeHead( 200, { } );
	result.end( reply.jsonfy( ) );
};
