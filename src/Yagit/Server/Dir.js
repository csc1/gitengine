/*
| Handles dir requests.
|
| TRICK:
|   git diff 4b825dc642cb6eb9a060e54bf8d69288fbee4904 --numstat GIT_REF -- FILEPATH
|
| WAIT: need to wait for git 2.43 it seems
|   https://stackoverflow.com/questions/66064549/git-diff-with-gitattributes-in-a-bare-repository
*/
def.abstract = true;

import { Self as Access            } from '{Yagit/Server/Access}';
import { Self as DirEntry          } from '{Yagit/Dir/Entry}';
import { Self as Git               } from '{Util/Git}';
import { Self as Https             } from '{Https/Self}';
import { Self as ListDirEntry      } from '{list@Yagit/Dir/Entry}';
import { Self as ReplyDir          } from '{Yagit/Reply/Dir}';
import { Self as RepositoryManager } from '{Repository/Manager}';

/*
| Handles a file request.
*/
def.static.handle =
	async function( request, result, path )
{
	const parts = path.parts;

	const plen = parts.length;
	if( plen < 3 )
	{
		return Https.error( result, 404, 'request too short' );
	}

/**/if( CHECK && parts.get( 0 ) !== 'dir' ) throw new Error( );

	const repoName = parts.get( 1 );

	if( !Access.test( request, result, repoName ) ) return;

	const repo = RepositoryManager.get( repoName );
	// repo must exist otherwise access would have denied
	const partCommitSha = parts.get( 2 ).trim( );
	if( partCommitSha[ 0 ] === '-' )
	{
		return Https.error( result, 404, 'invalid request' );
	}

	let data;
	try
	{
		data =
			await Git.command(
				repo.path,
				'--no-pager',
				'ls-tree',
				partCommitSha + ':' + parts.slice( 3 ).join( '/' )
			);
	}
	catch( e )
	{
		return Https.error( result, 404, 'not found' );
	}

	const list = [ ];
	const lines = data.split( '\n' );
	for( let line of lines )
	{
		line = line.trim( );
		if( line === '' ) continue;
		const iot = line.indexOf( '\t' );
		const info = line.substring( 0, iot );
		const filename = line.substring( iot + 1 );
		const iSplit = info.split( ' ' );

		if( iSplit[ 1 ] === 'tree' )
		{
			list.push(
				DirEntry.create(
					'name', filename,
					'type', 'dir',
				)
			);
			continue;
		}

		// tests if the file is in LFS
		try
		{
			data =
				await Git.command(
					repo.path,
					'--no-pager',
					'-c', 'attr.tree=' + partCommitSha,
					'check-attr', 'filter', '-z',
					'--', filename
				);
		}
		catch( e )
		{
			return Https.error( result, 500, 'internal server error' );
		}
		const dSplit = data.split( '\0' );

		if( dSplit[ 2 ] === 'lfs' )
		{
			list.push(
				DirEntry.create(
					'name', filename,
					'type', 'binary',
				)
			);
			continue;
		}

		// it might be a binary in git directly, use this following trick to see
		// if a diff to the zero commit has line numbers (text) or not (binary)

		try
		{
			data =
				await Git.command(
					repo.path,
					'--no-pager',
					'diff-tree',
					'--numstat',
					'4b825dc642cb6eb9a060e54bf8d69288fbee4904',
					partCommitSha,
					'--',
					filename
				);
		}
		catch( e )
		{
			return Https.error( result, 500, 'internal server error' );
		}

		list.push(
			DirEntry.create(
				'name', filename,
				'type',
					data[ 0 ] === '-'
					? 'binary'
					: 'text'
			)
		);
	}

	const headers = { };
	// 28 days caching (git commit version shouldn't ever change)
	//headers[ 'Cache-Control' ] = 'max-age=2419200';
	headers[ 'Cache-Control' ] = 'max-age=86400';

	const reply = ReplyDir.create( 'entries', ListDirEntry.Array( list ) );
	result.writeHead( 200, headers );
	result.end( reply.jsonfy( ) );
};
