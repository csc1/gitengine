/*
| Returns the diffs of a commit (to it's parent).
*/
def.abstract = true;

import { Self as Access            } from '{Yagit/Server/Access}';
import { Self as Diffs             } from '{Yagit/Commit/Diffs}';
import { Self as Git               } from '{Util/Git}';
import { Self as Https             } from '{Https/Self}';
import { Self as Hunk              } from '{Yagit/Commit/Patch/Hunk/Self}';
import { Self as Line              } from '{Yagit/Commit/Patch/Hunk/Line}';
import { Self as ListDiffs         } from '{list@Yagit/Commit/Diffs}';
import { Self as ListHunk          } from '{list@Yagit/Commit/Patch/Hunk/Self}';
import { Self as ListLine          } from '{list@Yagit/Commit/Patch/Hunk/Line}';
import { Self as ListPatch         } from '{list@Yagit/Commit/Patch/Self}';
import { Self as Patch             } from '{Yagit/Commit/Patch/Self}';
import { Self as RepositoryManager } from '{Repository/Manager}';

const utf8Decoder = new TextDecoder( 'utf-8' );
const ui82 = new Uint8Array( 2 );

/*
| Parses the raw header.
|
| ~return: [ array of patches, lookup-map ]
|          lookup-map is filenameOld -> filenameNew -> offset in patches
*/
function parseSectionRaw( lines )
{
	const patches = [ ];
	const lookup = new Map( );

	for( let lc = 0, llen = lines.length; lc < llen; lc++ )
	{
		let line = lines[ lc ];
		if( line === '' ) break;

		if( line[ 0 ] !== ':' )
		{
			console.log( 'line does not start with ":"' );
			return [ ];
		}

		// next 4 words in line are mode, mode, sha-from, sha-to
		// ... not cared about
		const spl = line.split( ' ' );
		const a4n = spl[ spl.length - 1 ];

		let filenameOld;
		let filenameNew;
		switch( a4n[ 0 ] )
		{
			case 'A':
				// addition of a file
				filenameNew = lines[ ++lc ];
				break;

			case 'C':
				// copy of a file into a new one
				filenameNew = lines[ ++lc ];
				break;

			case 'D':
				// deletion of a file
				filenameOld = lines[ ++lc ];
				break;

			case 'M':
				// modification of the contents or mode of a file
				filenameOld = lines[ ++lc ];
				filenameNew = lines[ lc ];
				break;

			case 'R':
				// renaming of a file
				filenameOld = lines[ ++lc ];
				filenameNew = lines[ ++lc ];
				break;

			case 'T':
				// change in the type of the file (regular file, symbolic link or submodule)
				// ignore it for now..
				continue;

			default:
			// also 'U' and 'X', never should happen here
				console.log( 'unknown status:', a4n[ 0 ] );
				return [ ];
		}
		const offset = patches.length;
		patches.push( { new: filenameNew, old: filenameOld, hunks: [ ] } );
		let lo = lookup.get( filenameOld );
		if( !lo )
		{
			lo = new Map( );
			lookup.set( filenameOld, lo );
		}
		lo.set( filenameNew, offset );
	}

	return [ patches, lookup ];
}

/*
| parses a git quote path
*/
function parseQuotePath( str )
{
	let s = '';
	for( let i = 1, iLen = str.length - 1; i < iLen; i++ )
	{
		const c = str.charAt( i );

		if( c !== '\\' )
		{
			s += c;
			continue;
		}

		const c1 = str.charAt( ++i );
		switch( c1 )
		{
			case 'n':
				s += '\n';
				continue;

			case 't':
				s += '\t';
				continue;
		}

		// it needs to be a double utf-8 octal value
		if( c1 < '0' || c1 > '8' )
		{
			console.log( 'malformed quote path (c1):', str );
			return undefined;
		}

		const c2 = str.charAt( ++i );
		const c3 = str.charAt( ++i );
		const c4 = str.charAt( ++i );
		const c5 = str.charAt( ++i );
		const c6 = str.charAt( ++i );
		const c7 = str.charAt( ++i );

		if( c2 < '0' || c2 > '8' )
		{
			console.log( 'malformed quote path (c2):', str );
			return undefined;
		}
		if( c3 < '0' || c3 > '8' )
		{
			console.log( 'malformed quote path (c3):', str );
			return undefined;
		}

		if( c4 !== '\\' )
		{
			console.log( 'malformed quote path (c4):', str );
			return undefined;
		}

		if( c5 < '0' || c5 > '8' )
		{
			console.log( 'malformed quote path (c5):', str );
			return undefined;
		}
		if( c6 < '0' || c6 > '8' )
		{
			console.log( 'malformed quote path (c6):', str );
			return undefined;
		}
		if( c5 < '0' || c7 > '8' )
		{
			console.log( 'malformed quote path (c7):', str );
			return undefined;
		}

		ui82[ 0 ] = (c1-'0') * 64 + (c2-'0') * 8 + (c3-'0');
		ui82[ 1 ] = (c5-'0') * 64 + (c6-'0') * 8 + (c7-'0');

		s += utf8Decoder.decode( ui82 );
	}

	return s;
}

/*
| Parses the raw patch hunks.
|
| ~lines:      lines of the patches section
| ~patches: array of patches of the raw section
| ~lookup:    lookup for the offset into the rawPatches
*/
function parseSectionPatches( lines, patches, lookup )
{
	//for( let lc = 0, llen = lines.length; lc < llen; lc++ )
	//{
	//	console.log( 'PSP ' + lc + ':>' + lines[ lc ] + '<' );
	//}

	for( let lc = 0, llen = lines.length; lc < llen; lc++ )
	{
		let line = lines[ lc ];
		if( !line.startsWith( '--- ' ) ) continue;

		let filenameFrom = line.substring( 4 );

		// Hack, git sometimes adds a \t to end of it ???
		filenameFrom = filenameFrom.trim( );

		if( filenameFrom.startsWith( '"' ) && filenameFrom.endsWith( '"' ) )
		{
			filenameFrom = parseQuotePath( filenameFrom );
			if( filenameFrom === undefined )
			{
				return undefined;
			}
		}

		if( filenameFrom === '/dev/null' )
		{
			filenameFrom = undefined;
		}
		else
		{
			if( !filenameFrom.startsWith( 'a/' ) )
			{
				console.log( 'patch malformed line', lc, 'expected "a/", but got:', filenameFrom );
				return undefined;
			}
			filenameFrom = filenameFrom.substring( 2 );
		}

		line = lines[ ++lc ];
		if( !line || !line.startsWith( '+++ ' ) )
		{
			console.log( 'patch malformed, expected "+++ "' );
			return undefined;
		}

		let filenameTo = line.substring( 4 );

		// Hack, git sometimes adds a \t to end of it ???
		filenameTo = filenameTo.trim( );

		if( filenameTo.startsWith( '"' ) && filenameTo.endsWith( '"' ) )
		{
			filenameTo = parseQuotePath( filenameTo );
			if( filenameTo === undefined )
			{
				return undefined;
			}
		}

		if( filenameTo === '/dev/null' )
		{
			filenameTo = undefined;
		}
		else
		{
			if( !filenameTo.startsWith( 'b/' ) )
			{
				console.log( 'patch malformed line', lc, 'expected "b/", but got:', filenameTo );
				return undefined;
			}
			filenameTo = filenameTo.substring( 2 );
		}

		const lo = lookup.get( filenameFrom );
		if( lo === undefined )
		{
			console.log( 'patch malformed, --- "' + filenameFrom + '" not in raw' );
			return undefined;
		}

		const offset = lo.get( filenameTo );
		if( offset === undefined )
		{
			console.log( 'patch malformed, +++ "' + filenameTo + '" not in raw' );
			return undefined;
		}
		const patch = patches[ offset ];

		while( true )
		{
			line = lines[ ++lc ];
			if( !line || !line.startsWith( '@@' ) )
			{
				console.log( 'patch malformed, expected "@@ "' );
				return undefined;
			}

			const spHunk = line.split( ' ' );
			if( spHunk.length < 4 )
			{
				console.log( 'spHunk.length < 4' );
				return undefined;
			}

			let spHunkOld = spHunk[ 1 ];
			let spHunkNew = spHunk[ 2 ];

			if( spHunkOld[ 0 ] !== '-' || spHunkNew[ 0 ] !== '+' )
			{
				console.log( 'spHunk malformed' );
				return undefined;
			}

			spHunkOld = spHunkOld.split( ',' )[ 0 ];
			spHunkNew = spHunkNew.split( ',' )[ 0 ];

			let lineOld = Math.abs( parseInt( spHunkOld, 10 ) );
			let lineNew = Math.abs( parseInt( spHunkNew, 10 ) );

			const linesHunk = [ ];
			let loopLines = true;
			// loop linues in hunk
			while( loopLines )
			{
				line = lines[ ++lc ];
				if( !line ) break;

				switch( line[ 0 ] )
				{
					case '-':
						linesHunk.push(
							Line.create(
								'oldLineno', lineOld++,
								'newLineno', -1,
								'content', line.substring( 1 ),
							)
						);
						break;

					case '+':
						linesHunk.push(
							Line.create(
								'oldLineno', -1,
								'newLineno', lineNew++,
								'content', line.substring( 1 ),
							)
						);
						break;

					case ' ':
						linesHunk.push(
							Line.create(
								'oldLineno', lineOld++,
								'newLineno', lineNew++,
								'content', line.substring( 1 ),
							)
						);
						break;

					default:
						loopLines = false;
						break;
				}
			}
			patch.hunks.push(
				Hunk.create( 'lines', ListLine.Array( linesHunk ) )
			);

			// checks if there are more hunks in this patch
			if( lines[ lc ].startsWith( '@@ ' ) )
			{
				lc--;
			}
			else
			{
				break;
			}
		}
	}

	const patchesIA = [ ];

	for( let patch of patches )
	{
		patchesIA.push(
			Patch.create(
				'newFile', patch.new,
				'oldFile', patch.old,
				'hunks', ListHunk.Array( patch.hunks ),
			)
		);
	}

	return ListPatch.Array( patchesIA );
}

/*
| Handles a diffs request.
*/
def.static.handle =
	async function( request, result, path )
{
	const parts = path.parts;

	const plen = parts.length;
	if( plen !== 3 )
	{
		return Https.error( result, 404, 'invalid request length' );
	}

/**/if( CHECK && parts.get( 0 ) !== 'diffs' ) throw new Error( );

	const repoName = parts.get( 1 );

	if( !Access.test( request, result, repoName ) ) return;

	// repo must exist otherwise access would have denied
	const repo = RepositoryManager.get( repoName );

	const partCommitSha = parts.get( 2 ).trim( );
	if( partCommitSha[ 0 ] === '-' )
	{
		return Https.error( result, 404, 'invalid request' );
	}

	// first gets the parents of the commit
	let data;
	try
	{
		data =
			await Git.command(
				repo.path,
				'--no-pager',
				'rev-list',
				'--no-commit-header',
				'-n', '1',
				'--format=%P',
				partCommitSha
			);
	}
	catch( e )
	{
		return Https.error( result, 500, 'internal server error' );
	}

	let spParents = data.split( '\n' )[ 0 ].split( ' ' );
	if( spParents[ 0 ] === '' )
	{
		// if this is the first commit add the special "none" id as parent
		spParents.unshift( '4b825dc642cb6eb9a060e54bf8d69288fbee4904' );
	}

	// diffs for all parents
	const diffs = [ ];

	for( let shaParent of spParents )
	{
		if( shaParent === '' ) continue;

		try
		{
			data =
				await Git.command(
					repo.path,
					'--no-pager',
					'diff-tree',
					'--patch-with-raw',
					'-z',
					'--find-renames',
					shaParent,
					partCommitSha,
				);
		}
		catch( e )
		{
			return Https.error( result, 500, 'internal server error' );
		}

		if( data === '' )
		{
			diffs.push(
				Diffs.create( 'patches', ListPatch.Empty )
			);
			continue;
		}

		const [ dataRaw, dataPatches ] = data.split( '\0\0' );
		const [ patchesRaw, lookup ] = parseSectionRaw( dataRaw.split( '\0' ) );
		if( !patchesRaw )
		{
			return Https.error( result, 500, 'internal server error' );
		}

		const patches = parseSectionPatches( dataPatches.split( '\n' ), patchesRaw, lookup );
		if( !patches )
		{
			return Https.error( result, 500, 'internal server error' );
		}

		diffs.push(
			Diffs.create( 'patches', ListPatch.Array( patches ) )
		);
	}

	const reply = ListDiffs.Array( diffs );

	const headers = { };
	// 28 days caching (git commit version shouldn't ever change)
	//headers[ 'Cache-Control' ] = 'max-age=2419200';
	headers[ 'Cache-Control' ] = 'max-age=86400';
	headers[ 'Content-Type' ] = 'application/json';
	headers[ 'Date' ] = new Date().toUTCString( );

	result.writeHead( 200, headers );
	result.end( reply.jsonfy( ) );
};
