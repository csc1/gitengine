/*
| Handles history requests.
|
| FIXME cache history graphs.
*/
def.abstract = true;

import { Self as Access            } from '{Yagit/Server/Access}';
import { Self as Commit            } from '{Yagit/Commit/Self}';
import { Self as CommitRef         } from '{Yagit/Commit/Ref}';
import { Self as Git               } from '{Util/Git}';
import { Self as Https             } from '{Https/Self}';
import { Self as ListCommit        } from '{list@Yagit/Commit/Self}';
import { Self as ListCommitRef     } from '{list@Yagit/Commit/Ref}';
import { Self as ReplyHistory      } from '{Yagit/Reply/History}';
import { Self as RepositoryManager } from '{Repository/Manager}';

/*
| Handles a history request.
*/
def.static.handle =
	async function( request, result, path )
{
	const parts = path.parts;

	const plen = parts.length;
	if( plen !== 5 )
	{
		return Https.error( result, 404, 'invalid history request' );
	}

/**/if( CHECK && parts.get( 0 ) !== 'history' ) throw new Error( );

	const repoName = parts.get( 1 );

	if( !Access.test( request, result, repoName ) ) return;

	const partCommitSha = parts.get( 2 ).trim( );
	if( partCommitSha[ 0 ] === '-' )
	{
		return Https.error( result, 404, 'invalid history request' );
	}

	const pStartAt = parts.get( 3 ).trim( );
	const pStopAt = parts.get( 4 ).trim( );
	let startAt = parseInt( pStartAt, 10 );
	let stopAt;

	if( pStartAt !== '' + startAt || startAt < 0 )
	{
		return Https.error( result, 404, 'invalid start at' );
	}

	if( pStopAt !== 'all' )
	{
		stopAt = parseInt( pStopAt, 10 );
		if( pStopAt !== '' + stopAt || stopAt < 0 )
		{
			return Https.error( result, 404, 'invalid stop at' );
		}

		if( stopAt <= startAt )
		{
			return Https.error( result, 404, 'invalid start stop range' );
		}
	}

	const repo = RepositoryManager.get( repoName );
	// repo must exist otherwise access would have denied

	let data;
	try
	{
		data =
			await Git.command(
				repo.path,
				'--no-pager',
				'rev-list',
				'--no-commit-header',
				'--format=%H%x00%P%x00%an%x00%at%x00%s',
				partCommitSha
			);
	}
	catch( e )
	{
		return Https.error( result, 500, 'internal server error' );
	}

	const lines = data.split( '\n' );

	// lookup table for SHAs in the commit;
	const lookup = { };
	let c = 0;
	for( let line of lines )
	{
		if( line === '' ) continue;
		const sp = line.split( '\0' );
		lookup[ sp[ 0 ] ] = c++;
	}

	// initial fill of the commit list
	const commits = [ ];
	for( let line of lines )
	{
		if( line === '' ) continue;
		const sp = line.split( '\0' );

		const spParents = sp[ 1 ].split( ' ' );
		const parents = [ ];
		for( let shaP of spParents )
		{
			if( shaP === '' ) continue;

			const offset = lookup[ shaP ];
			if( offset === undefined )
			{
				return Https.error( result, 500, 'internal server error' );
			}

			parents.push(
				CommitRef.create(
					'sha', shaP,
					'offset', offset,
				),
			);
		}

		commits.push(
			Commit.create(
				'sha',        sp[ 0 ],
				'authorName', sp[ 2 ],
				'date',       parseInt( sp[ 3 ], 10 ) * 1000,
				'message',    sp[ 4 ],
				'parents',    ListCommitRef.Array( parents ),
				'graphLevel', 0,
			)
		);
	}

	// stage 1: calculates graph levels
	for( let c = 0, clen = commits.length; c < clen; c++ )
	{
		const commit = commits[ c ];
		for( let pc of commit.parents )
		{
			const opc = pc.offset;
			for( let cc = c + 1; cc < opc; cc++ )
			{
				const co = commits[ cc ];
				if( co.graphLevel >= commit.graphLevel )
				{
					commits[ cc ] = co.create( 'graphLevel', co.graphLevel + 1 );
				}
			}
		}
	}

	// stage 2: fixes potential overlaps
	let collision;
	do
	{
		collision = false;
		for( let c = 0, clen = commits.length; c < clen; c++ )
		{
			let commit = commits[ c ];
			for( let pc of commit.parents )
			{
				const opc = pc.offset;
				for( let cc = c + 1; cc < opc; cc++ )
				{
					const co = commits[ cc ];
					if( co.graphLevel === commit.graphLevel )
					{
						collision = true;
						console.log( 'fixed history collision' );
						commit = commits[ c ] = commit.create( 'graphLevel', commit.graphLevel + 1 );
					}
				}
			}
		}
	} while( collision );

	// fills in parent graphLevel information
	for( let c = 0, clen = commits.length; c < clen; c++ )
	{
		const commit = commits[ c ];
		const parents = [ ];
		for( let p of commit.parents )
		{
			parents.push(
				p.create( 'graphLevel', commits[ p.offset ].graphLevel )
			);
		}
		commits[ c ] = commit.create( 'parents', ListCommitRef.Array( parents ) );
	}

	let rCommits = commits.slice( startAt, pStopAt === 'all' ? Number.Infinity : stopAt );

	const reply =
		ReplyHistory.create(
			'commits', ListCommit.Array( rCommits ),
			'offset', startAt,
			'repository', repoName,
			'total', commits.length,
		);

	const headers = { };
	// 28 days caching (git commit version shouldn't ever change)
	//headers[ 'Cache-Control' ] = 'max-age=2419200';
	headers[ 'Cache-Control' ] = 'max-age=86400';
	headers[ 'Content-Type' ] = 'application/json';
	headers[ 'Date' ] = new Date().toUTCString( );

	result.writeHead( 200, headers );
	result.end( reply.jsonfy( ) );
};
