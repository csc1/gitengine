/*
| yagit - yet another git interface.
|
|          root.
*/
def.abstract = true;

import { Self as Log            } from '{Log/Self}';
import { Self as SessionManager } from '{Yagit/Session/Manager}';
import { Self as Web            } from '{Yagit/Server/Web}';

/*
| The ti2cWeb middleware.
*/
let _web;

/*
| Initializes yagit root.
*/
def.static.init =
	async function( dir )
{
	Log.log( 'yagit', '*', 'init' );
	_web = await Web.prepare( dir );
	SessionManager.init( );
	Log.log( 'yagit', '*', 'prepared' );
};

/*
| Serves a yagit request.
|
| ~count: client counter
| ~req: request
| ~res: result
| ~urlSplit: url splitted into parts
*/
def.static.serve =
	async function( count, req, res, urlSplit )
{
	_web.serve( count, req, res, urlSplit );
};
