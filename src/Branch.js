/*
| The data about a git branch.
*/
def.attributes =
{
	// branch name
	name: { type: 'string' },

	// last commit hash
	lastCommitHash: { type: [ 'undefined', 'string' ] },
};
