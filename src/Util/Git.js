/*
| Git tools.
*/
def.abstract = true;

import child_process from 'node:child_process';

/*
| Executes a git command.
|
| ~gitDir: the git directory
| ~args:   the git arguments
*/
def.static.command =
	function( gitDir, ...args )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length < 2 ) throw new Error( );
/**/	if( typeof( gitDir ) !== 'string' ) throw new Error( );
/**/}

	const out = [ ];
	const err = [ ];

	const child = child_process.spawn(
		'/usr/bin/git', args,
		{ cwd: gitDir },
	);
	child.stdout.on( 'data', ( data ) => out.push( data + '' ) );
	child.stderr.on( 'data', ( data ) => err.push( data + '' ) );

	return new Promise( ( resolve, reject ) =>
	{
		const e = new Error( );
		child.on(
			'close',
			( code ) =>
		{
			const outs = out.join( '' );
			const errs = err.join( '' );
			if( code === 0 )
			{
				resolve( outs );
			}
			else
			{
				console.log( 'git error', outs, errs );
				e.message = 'git error: ' + errs;
				reject( e );
			}
		} );
	} );
};

/*
| Executes a git command for a binary answer stream.
|
| ~gitDir: the git directory
| ~args:   the git arguments
*/
def.static.commandBinary =
	function( gitDir, ...args )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length < 2 ) throw new Error( );
/**/	if( typeof( gitDir ) !== 'string' ) throw new Error( );
/**/}

	const out = [ ];
	const err = [ ];

	const child = child_process.spawn(
		'/usr/bin/git', args,
		{ cwd: gitDir },
	);
	child.stdout.on( 'data', ( data ) => out.push( data ) );
	child.stderr.on( 'data', ( data ) => err.push( data ) );

	return new Promise( ( resolve, reject ) =>
	{
		const e = new Error( );
		child.on(
			'close',
			( code ) =>
		{
			if( code === 0 )
			{
				resolve( Buffer.concat( out ) );
			}
			else
			{
				const outs = out.join( '' );
				const errs = err.join( '' );
				console.log( 'git error', outs, errs );
				e.message = 'git error: ' + errs;
				reject( e );
			}
		} );
	} );
};
